﻿Public Class Form1
    Dim x As Integer
    Dim y As Integer


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            x = Convert.ToInt32(TextBox1.Text)
            y = Convert.ToInt32(TextBox2.Text)
            If x = y Then
                MsgBox("Son iguales", MsgBoxStyle.Critical)
            Else
                MsgBox("No son iguales", MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            MsgBox("Entrada ivalida", MsgBoxStyle.Critical)
        End Try
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            x = Convert.ToInt32(TextBox1.Text)
            y = Convert.ToInt32(TextBox2.Text)
            If x < y Then
                MsgBox("Numero 1 es menor a Numero 2", MsgBoxStyle.Critical)
            Else
                MsgBox("Numero 1 no es menor a Numero 2", MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            MsgBox("Entrada ivalida", MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            x = Convert.ToInt32(TextBox1.Text)
            y = Convert.ToInt32(TextBox2.Text)
            If x > y Then
                MsgBox("Numero 1 es mayor a Numero 2", MsgBoxStyle.Critical)
            Else
                MsgBox("Numero 1 no es mayor a Numero 2", MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            MsgBox("Entrada ivalida", MsgBoxStyle.Critical)
        End Try
    End Sub
End Class
