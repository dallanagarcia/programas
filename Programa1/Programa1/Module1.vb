﻿Module Module1


    Sub Main()
        Console.WriteLine("Escribe una palabra")
        Dim nombre As String = Console.ReadLine()

        Dim intCount As Integer

        For intCount = 0 To Len(nombre) - 1

            If intCount Mod 2 = 0 Then
                Console.WriteLine("")
                Console.Write(nombre.Chars(intCount))
            Else
                Console.Write(nombre.Chars(intCount))
            End If

        Next intCount
        Console.ReadKey()
    End Sub

End Module
