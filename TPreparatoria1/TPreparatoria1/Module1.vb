﻿Imports System.IO
Module Module1

    Sub Main()
        Dim n, cont, mul, t As Integer
        Dim divisor, cociente, producto As Integer

        Dim Opcion As Char = ""
        While Opcion <> "6"
            Console.WriteLine("****************************************")
            Console.WriteLine("Dallana Isabely Garcia Celis 201709217 ")
            Console.WriteLine("****************************************")
            Console.WriteLine()
            Console.WriteLine("---------- Bienvenido -------------")
            Console.WriteLine("    Tabla de multiplicar......(1) ")
            Console.WriteLine("    Divisores.................(2) ")
            Console.WriteLine("    Vocales...................(3) ")
            Console.WriteLine("    Letra elegida.............(4) ")
            Console.WriteLine("    Grabar en archivo texto...(5) ")
            Console.WriteLine("    Salir.....................(6) ")
            Try
                Opcion = Console.ReadKey.KeyChar
                Console.Clear()
                Select Case Opcion
                    Case "1"
                        Console.WriteLine("****************************************")
                        Console.WriteLine("Dallana Isabely Garcia Celis 201709217 ")
                        Console.WriteLine("****************************************")
                        Console.WriteLine("Ingrese el numero que desea su tabla:")
                        n = Console.ReadLine
                        Console.WriteLine("Ingrese el tamaño de su tabla de multiplicar:")
                        t = Console.ReadLine
                        Console.WriteLine("Tabla de Multiplicar del numero {0}", n)

                        For cont = 0 To t Step 1

                            mul = n * cont

                            Console.WriteLine("{0} x {1} = {2}", n, cont, mul)
                        Next
                        Console.ReadKey()
                        Console.Clear()
                    Case "2"
                        Console.WriteLine("****************************************")
                        Console.WriteLine("Dallana Isabely Garcia Celis 201709217 ")
                        Console.WriteLine("****************************************")
                        Console.WriteLine()
                        Console.WriteLine("Ingrese el numero que desea ver sus divisores")
                        n = Console.ReadLine
                        For divisor = 2 To n
                            cociente = n \ divisor
                            producto = cociente * divisor
                            If producto = n Then
                                Console.WriteLine(divisor & " es un divisor de " & n)
                            End If
                        Next
                        Console.ReadKey()
                        Console.Clear()
                    Case "3"
                        Console.WriteLine("****************************************")
                        Console.WriteLine("Dallana Isabely Garcia Celis 201709217 ")
                        Console.WriteLine("****************************************")
                        Console.WriteLine()

                        Dim count, count2 As Integer
                        Dim celda, cadena As String

                        Console.WriteLine("")
                        Console.WriteLine("Ingrese una frase")
                        cadena = Console.ReadLine
                        Console.WriteLine("")

                        For I = 1 To cadena.Length

                            celda = Mid(cadena, I, 1)

                            If LCase(celda) = "a" Or LCase(celda) = "e" Or LCase(celda) = "i" Or LCase(celda) = "o" Or LCase(celda) = "u" Then
                                count = count + 1

                            Else
                                count2 = count2 + 1
                            End If

                        Next

                        Console.WriteLine("")
                        Console.WriteLine("La cantidad de vocales que contiene la frase es de:")
                        Console.WriteLine(count)
                        Console.WriteLine("")
                        Console.WriteLine("Presione enter para regresar al menu")
                        Console.ReadLine()
                        Console.Clear()
                    Case "4"
                        Console.WriteLine("****************************************")
                        Console.WriteLine("Dallana Isabely Garcia Celis 201709217 ")
                        Console.WriteLine("****************************************")
                        Console.WriteLine()
                        Dim count As Integer
                        Dim celda, cadena, letra As String

                        Console.WriteLine("Ingrese una frase")
                        cadena = Console.ReadLine
                        Console.WriteLine("")
                        Console.WriteLine("Ingrese la letra que desea ver cuantas veces se repite en la frase")
                        letra = Console.ReadLine
                        Console.WriteLine("")

                        For i = 1 To cadena.Length

                            celda = Mid(cadena, i, 1)

                            If celda = letra Then
                                count = count + 1
                            End If

                        Next

                        Console.WriteLine("")
                        Console.WriteLine("La cantidad de la letra de ingresada en la frase es de:")
                        Console.WriteLine(count)
                        Console.WriteLine("")
                        Console.WriteLine("Presione enter para regresar al menu")
                        Console.ReadLine()
                        Console.Clear()


                    Case "5"
                        Console.WriteLine("****************************************")
                        Console.WriteLine("Dallana Isabely Garcia Celis 201709217 ")
                        Console.WriteLine("****************************************")
                        Console.WriteLine()
                        Dim nombre, cadena As String

                        Console.WriteLine("Ingrese una Nombre para el archivo")
                        nombre = Console.ReadLine
                        Console.WriteLine("")
                        Console.WriteLine("Ingrese Una frase")
                        cadena = Console.ReadLine
                        Console.WriteLine("")

                        Dim doc As StreamWriter
                        doc = New StreamWriter("C:\Users\dalla\Documents\" + nombre + ".txt")
                        doc.WriteLine(cadena)
                        doc.Close()

                        Console.WriteLine("")
                        Console.WriteLine("El archivo se a creado exitosamente, esta ubicado en Documentos")
                        Console.WriteLine("")
                        Console.WriteLine("Presione enter para regresar al menu")
                        Console.ReadLine()
                        Console.Clear()


                    Case "6"
                        Console.Clear()
                        Console.WriteLine("****************************************")
                        Console.WriteLine("Dallana Isabely Garcia Celis 201709217 ")
                        Console.WriteLine("****************************************")
                        Console.WriteLine()
                        Console.WriteLine("Gracias por utilizar nuestra plataforma")
                        Console.ReadKey()
                    Case Else
                        Console.Clear()
                        Console.WriteLine("Opcion ingresada no es valida, vuelva a intentar")

                End Select
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try
            If Opcion <> "6" Then
                Console.WriteLine("Pulse cualquier teclara para regresar al menu")
                Console.ReadKey()
            End If
        End While
    End Sub




End Module
