﻿Module Module1

    Sub Main()
        Dim n As Integer
        Dim cont As Integer
        Dim mul As Integer
        Console.WriteLine("Ingrese el numero que desea su tabla:")
        n = Console.ReadLine

        Console.WriteLine("Tabla de Multiplicar del numero {0}", n)

        For cont = 1 To 10 Step 1

            mul = n * cont

            Console.WriteLine("{0} x {1} = {2}", n, cont, mul)
        Next
        Console.ReadKey()

    End Sub

End Module
